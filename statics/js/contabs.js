/*
 *
 *   H+ - 后台主题UI框架
 *   version 4.9
 *
*/

$(function () {
    function menuItem() {
        // 获取标识数据
        var dataUrl = $(this).attr('href'),
            flag = true;
        if (dataUrl == undefined || $.trim(dataUrl).length == 0)return false;

        // 选项卡菜单不存在
        if (flag) {
            // 添加选项卡对应的iframe
            var str1 = '<iframe class="J_iframe" width="100%" height="100%" src="' + dataUrl + '" frameborder="0" data-id="' + dataUrl + '" seamless></iframe>';
            var p = $('.J_mainContent');
            p.empty();
            p.append(str1);

        }
        return false;
    }

    $('.J_menuItem').on('click', menuItem);
    
});
