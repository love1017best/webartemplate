# VR/AR + 教育实验室 WebAR项目 静态页面模板

模板文件位于本项目下 `WEB-INF/demos` 路径内，现有模板：

1. `single.html` 简单的单标记 AR 应用模板
2. `multiple.html` 多标记 AR 应用模板
3. `coins.html` 带交互的复杂多标记 AR 应用模板（以抛硬币为例）
4. `distance.html` 测量两个识别图之间距离的模板
5. NEW:`pair.html` 同时识别两个识别图才出现一个模型的模板

此外，`generator.html` 文件可用于生成符合要求的识别图文件。详细说明见[识别图制作（patt 格式）](#%e8%af%86%e5%88%ab%e5%9b%be%e5%88%b6%e4%bd%9cpatt-%e6%a0%bc%e5%bc%8f)。

需要的测试环境：

1. Web 服务器，如 Apache 或 Nginx，如果不了解详情，请直接安装 xampp，详见[部署方法](#%e9%83%a8%e7%bd%b2%e6%96%b9%e6%b3%95)
2. Chrome/Firefox/Safari 等现代浏览器，不要使用 IE 测试

# 部署方法

将整个项目完整文件夹置于 web 服务器的访问路径下，**通过 HTTP 协议访问。**

建议使用 xampp（[https://www.apachefriends.org/index.html](https://www.apachefriends.org/index.html)）。下载安装 xampp 后，将完整的 `WebARTemplate` 文件夹放置于 xampp 安装路径下的 `htdocs` 文件夹中，启动 xampp 控制面板，启动 Apache 服务器，通过浏览器访问 [http://localhost/WebARTemplate](http://localhost/WebARTemplate) 即可看到对应的文件目录树。

![](resources/1.png)

**注意：一定要通过浏览器用 HTTP 协议访问网站，不能直接双击 html 文件用浏览器打开，否则部分关键组件不会工作，导致功能不正常。**

# 模板使用方法介绍

**在制作自己的作品时，请不要修改模板本身，可以复制一份重命名进行修改。**

以简单模板 `single.html` 为例，关键代码位于 `<body>` 标签下的 `<a-scene>` 标签内，`<a-marker>` 标签代表一张识别图，里面的 `<a-plane>` 标签为固定格式不需改动，`<a-gltf-model>` 标签为识别图对应的 3D 模型，格式为 glTF。

![](resources/3.png)

在 WebAR 中识别图由一个纯文本的点阵文件 patt 文件描述，路径定义在 <a-marker> 标签内的 url 属性，实际文件存储于 `/upload/patt` 路径下。如果需要更换识别图，先生成自己的识别图 patt 文件，再将其放入 `/upload/patt` 文件夹中，在 `<a-marker>` 标签中更改对应文件名即可。关于识别图的制作方法见[识别图制作（patt 格式）](#%e8%af%86%e5%88%ab%e5%9b%be%e5%88%b6%e4%bd%9cpatt-%e6%a0%bc%e5%bc%8f)。

3D 模型请使用 glTF 格式，是一种开源的 3D 模型格式。模型的路径定义于 `<a-gltf-model>` 标签内的 src 和 gltf-model 两个属性值，实际文件存储于 /Models 路径下。每个 glTF 模型应该独立存储于一个文件夹中，并包含一个 .gltf 文件、一个 .bin 文件和一个 textures 文件夹。如果需要更换模型，先找到适合的 glTF 格式模型，再将其整个文件夹放入 `/Models` 文件夹中，在 `<a-gltf-model>` 标签中更改对应文件名即可，注意需要同时更改 src 和 gltf-model 两个属性的值。关于模型的制作见 [glTF 格式模型](#gltf-%e6%a0%bc%e5%bc%8f%e6%a8%a1%e5%9e%8b)。

多标记模板和复杂交互模板大致类似。

最终提交作品时，请提交修改后的 html 文件，对应的识别图（包括 png 格式带黑边和不带黑边的图片、patt 格式识别图）和 glTF 格式模型文件夹。请不要修改模板内的文件夹结构和 html 模板内的相对路径写法。

# 识别图制作（patt 格式）

识别图需要首先自行制作，然后使用特定的工具转换成 patt 格式。识别图需要为 1:1 的正方形图像，**制作时不带黑边，后续处理过程会自动添加黑边**，实例可见 `/WEB-INF/demos/inner-images` 下的 9 个文件。建议使用 PhotoShop 或其他工具制作 1:1 的浅灰色背景、黑色或其他颜色**文字**组成的图案，**不建议使用过于复杂的图像作为识别图**，保存成不低于 256*256 大小的 PNG 文件。

自己用 PhotoShop 等制作的识别图可参考：

![](WEB-INF/demos/inner-images/inner-arjs.png)

准备好原始识别图后，访问 `/WEB-INF/demos` 下的 `generator.html` 页面，上传自己制作好的正方形图片，其余选项保持默认，直接点击 DOWNLOAD MARKER 即可获得 patt 格式的识别图文件，点击 DOWNLOAD IMAGE 可以获得加上了黑白的 png 格式识别图。请下载两个文件并自己保存好。

![](resources/2.png)

请将 patt 文件放入 `/upload/patt` 路径内，在自己的作品中的 `<a-marker>` 标签内更改为新的识别图路径，例如 `../../upload/patt/MyImage.patt`，然后打印出带黑边的识别图用于识别。

最终提交时，请一并提交带黑边、不带黑边和 patt 三个识别图文件。请确保提交的识别图在运行时识别效果良好。

# glTF 格式模型

使用的模型不要过于复杂，体积也不要太大，以保证在 Web 环境下的体验。

一些 3D 模型资源网站例如 [Sketchfab](https://sketchfab.com/) 会提供 glTF 格式模型的下载，可以在 Sketchfab 注册账号搜索合适的模型下载直接使用。

![](resources/4.png)

当然也可以自行导出:

1. 从 3DS Max 导出 gltf 格式模型的官方教程：[https://doc.babylonjs.com/resources/3dsmax_to_gltf](https://doc.babylonjs.com/resources/3dsmax_to_gltf)
2. glTF 官方的 GitHub 仓库，其中的 glTF Tools 中提供了多种 gltf 格式的导出工具：[https://github.com/KhronosGroup/glTF#gltf-tools](https://github.com/KhronosGroup/glTF#gltf-tools)
3. fbx 格式的模型可以通过 Blender（[https://www.blender.org/](https://www.blender.org/)） 导出为 glTF 格式，导出时注意选择保存格式为 glTF Separate(.gltf + .bin + textures)

![](resources/7.png)

下载或者导出的 glTF 格式模型，其文件夹结构应如下：

![](resources/5.png)

可以用 Windows 10 自带的 3D 查看器直接打开 gltf 文件，在 .gltf 文件上右击，打开方式选择 3D 查看器即可预览：

![](resources/6.png)

预览正常的 3D 模型需要再放入 WebAR 页面中测试，将整个文件夹命名为有意义的名称后，放入 `/Models` 路径下，并在页面的 html 代码中更改对应路径，再通过浏览器访问测试，保证其在网页上能正常显示。

最终提交时，请提交一个文件夹，内含模型的 .gltf、.bin 文件和 textures 文件夹（部分模型可能没有 textures 文件夹）。请确保提交的模型能够在网页中的 AR 模式下正常显示。